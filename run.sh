#!/bin/bash
set -o nounset
set -o pipefail

# Arguments
readonly BASE_IMAGE="${1}"
readonly PLAYBOOK_FILE="${2}"
readonly PLAYBOOK_DATA_DIR="${3}"
readonly ARG_INVENTORY="${4}"
readonly ANSIBLE_TAGS="${5}"

# Static data
readonly SCRIPT_DIR=$(pwd)
readonly TEMP=$(mktemp -d)
readonly DOCKER_NAMESPACE="ansible-dep"

# Global variables
ERRORS=()

# Print message $2 with log-level $1 to STDERR, colorized if terminal
# https://gist.github.com/swaeku/6560309
log() {
    local level=${1?}
    shift
    local code= line="[$(date '+%F %T')] $level: $*"
    if [ -t 2 ]
    then
        case "$level" in
        INFO) code=36 ;;
        DEBUG) code=30 ;;
        WARN) code=33 ;;
        ERROR) code=31 ;;
        *) code=37 ;;
        esac
            echo -e "\e[${code}m${line}\e[0m"
        else
                echo "$line"
        fi >&2
}

fail() {
  log ERROR "${1}"
  exit -1
}

function cleanUp() {
    if [[ -d "${TEMP}" ]]
    then
        rm -r "${TEMP}"
    fi
}

trap cleanUp EXIT

prepareImage() {
    HOST="${1}"

    DIR="${TEMP}/${HOST}"
    mkdir -p "${DIR}"

    cp -r ${PLAYBOOK_DATA_DIR}/* ${DIR}
    cd ${DIR}
}

cleanupImage() {
    HOST="${1}"
    DIR="${TEMP}/${HOST}"

    cd "${SCRIPT_DIR}"
    rm -rf "${DIR}"

    IMAGE_NAME="${DOCKER_NAMESPACE}/${HOST}"
    docker rmi -f ${IMAGE_NAME}
}

buildImage() {
    TARGET_PATH="playbooks"
    HOST="${1}"
    PLAYBOOK="${TARGET_PATH}/${2}"
    INVENTORY="${TARGET_PATH}/${3}"

    echo "
      from ${BASE_IMAGE}
      add ./ /${TARGET_PATH}
      entrypoint [\"ansible-playbook\", \"--connection=local\", \"-i\", \"${INVENTORY}\", \"${PLAYBOOK}\"]
    " > Dockerfile

    docker build -t "${DOCKER_NAMESPACE}/${HOST}" .
}

runImage() {
    HOST="${1}"
    IMAGE_NAME="${DOCKER_NAMESPACE}/${HOST}"
    LOGFILE="${TEMP}/${DOCKER_NAMESPACE}_${HOST}.log"

    docker run  ${IMAGE_NAME} --syntax-check || fail "Syntax check for ${IMAGE_NAME} failed."
    docker run  ${IMAGE_NAME} --limit "${HOST}" --tags "${ANSIBLE_TAGS}" > "${LOGFILE}" && cat playbooks/log-collector.retry
    
    log INFO "Play returned `cat ${LOGFILE}`"
    FATALS=$(cat ${LOGFILE} | grep "failed=")
    
    if [[ ${FATALS} != "" ]]
    then
      log ERROR "Fatals ${FATALS}"
      ERRORS+=("${FATALS}")
    fi
}

main() {
    readonly HOSTS=$(cat "${PLAYBOOK_FILE}" | grep "hosts:" | awk '{print $3}')
    PLAYBOOK=$(basename ${PLAYBOOK_FILE})
    for GROUP in ${HOSTS};
    do
        log INFO "Working on ${GROUP}"
        prepareImage "${GROUP}"
        buildImage "${GROUP}" "${PLAYBOOK}" "${ARG_INVENTORY}"
        runImage "${GROUP}"
        cleanupImage "${GROUP}"
    done

    if [[ ${#ERRORS[@]} -gt 0 ]]
    then
      log ERROR "Encountered ${#ERRORS[@]} error(s): ${ERRORS[@]}"
      exit -1
    fi
}

main $@