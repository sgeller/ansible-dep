#!/bin/bash

main(){
  readonly image="${1}"
  readonly DIR="${PWD}"
  cd "base_images/${image}" && docker build -t ansible-dep/base-image-"${image}" . && cd "${DIR}"
}

main $@