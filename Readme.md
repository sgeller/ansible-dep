# Ansible-Dep

Provision an Ansible playbook into an enclosed environment to check

* validity of any syntax contained within the play
* all roles and tasks succeeded successfully

Basic flow:
For every group in the playbook
  run the play in its own container, limited to the group


# Requirements
* Tested with Docker version 1.10.2, build c3959b1

# Docker Base Images
Currently two base images are included
* ansible-dep/base-image-debian
* ansible-dep/base-image-centos

# Usage
Only once, generate the base image which contains Ansible
```
./prepare.sh [debian|centos]
```
Kick off Ansible-Dep for a playbook
```
./run.sh [ansible-dep/base-image-debian|ansible-dep/base-image-centos] playbook playbook_data_directory inventory
```

# Open issues
* Everything, just a proof-of-concept